const form = {
    id: "form-checkout",
    cardholderName: {
        id: "form-checkout__cardholderName",
        placeholder: "Holder name",
    },
    cardholderEmail: {
        id: "form-checkout__cardholderEmail",
        placeholder: "E-mail",
    },
    cardNumber: {
        id: "form-checkout__cardNumber",
        placeholder: "Card number",
        style: {
            fontSize: "1rem"
        },
    },
    expirationDate: {
        id: "form-checkout__expirationDate",
        placeholder: "MM/YYYY",
        style: {
            fontSize: "1rem"
        },
    },
    securityCode: {
        id: "form-checkout__securityCode",
        placeholder: "Security code",
        style: {
            fontSize: "1rem"
        },
    },
    installments: {
        id: "form-checkout__installments",
        placeholder: "Installments",
    },
    identificationType: {
        id: "form-checkout__identificationType",
    },
    identificationNumber: {
        id: "form-checkout__identificationNumber",
        placeholder: "Identification number",
    },
    issuer: {
        id: "form-checkout__issuer",
        placeholder: "Issuer",
    },
};

export default form